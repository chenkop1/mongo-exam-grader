const {
    answersFolder,
    resultsFileName,
    db: { url, dbName }
} = require('./config');

const fs = require('fs');
const { MongoClient } = require('mongodb');
const mongoOptions = { useUnifiedTopology: true, useNewUrlParser: true };
const grader = require('./grader/grader');
const { sum, values } = require('ramda');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const csvWriter = createCsvWriter({
    path: resultsFileName,
    header: [{ id: 'studentId', title: 'Student Id' }, { id: 'grade', title: 'grade' }]
});

MongoClient.connect(url, mongoOptions, async (err, client) => {
    console.assert(!err, err);
    const db = client.db(dbName);
    await checkExams(db);
    client.close();
});

const asyncMap = async (arr, callback) => {
    const mappedArr = [];
    for (let i = 0; i < arr.length; i++) {
        mappedArr.push(await callback(arr[i], i, arr));
    }
    return mappedArr;
};
const withoutSuffix = fileName => fileName.replace(/^(.*?)\.[^\.]*$/, '$1');

const checkExams = async db => {
    console.log('Start checking exams...');
    const answersFiles = fs.readdirSync(answersFolder);
    console.log(`Total: ${answersFiles.length} exams`);
    const studentsResults = await asyncMap(answersFiles, async (fileName, index) => {
        const results = await grader(db, fileName);
        const studentResults = {
            studentId: withoutSuffix(fileName),
            grade: Math.ceil(sum(values(results)))
        };
        console.log(
            `(${index + 1}/${answersFiles.length}) ${studentResults.studentId} - ${
                studentResults.grade
            }`
        );
        return studentResults;
    });
    try {
        await csvWriter.writeRecords(studentsResults);
        console.log('The grades(.csv) file was written successfully.');
    } catch (e) {
        console.log(`Error in writing students' results to file - ${e}`);
    }
};
