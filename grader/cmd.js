const { exec } = require('child_process');

const cmd = async command =>
    new Promise((res, rej) => {
        exec(command, (err, stdout, stderr) => {
            if (err) rej({ err, stderr });
            else res(stdout);
        });
    });

module.exports = { cmd };
