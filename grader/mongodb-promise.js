module.exports = collection => ({
    find: (selectors = {}, options = {}) =>
        new Promise((res, rej) =>
            collection
                .find(selectors, options)
                .toArray((err, result) => (err ? rej(err) : res(result)))
        ),
    safeDrop: () =>
        new Promise(res => collection.drop((err, result) => (err ? res(err) : res(result))))
});
