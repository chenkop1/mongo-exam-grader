const {
    answersFolder,
    db: { studentsCol: STUDENTS, teachersCol: TEACHERS },
    scores
} = require('../config');
const { cmd } = require('./cmd');
const {
    dissoc,
    head,
    pipe,
    groupBy,
    pluck,
    all,
    contains,
    sortBy,
    sort,
    prop,
    map,
    equals
} = require('ramda');
const mongo = require('./mongodb-promise');
const studentsData = require('./Questions data/Question1/students');
const teachersData = require('./Questions data/Question1/teachers');
const aggreagationData = require('./Questions data/Question2/students');
const solutions = require('./solutions');

const initDBForCrudQuestion = async db => {
    await mongo(db.collection(STUDENTS)).safeDrop();
    await mongo(db.collection(TEACHERS)).safeDrop();
    await db.createCollection(STUDENTS);
    await db.createCollection(TEACHERS);
    await db.collection(STUDENTS).insertMany(studentsData);
    await db.collection(TEACHERS).insertMany(teachersData);
};

const initDBForAggregationQuestion = async db => {
    await mongo(db.collection(STUDENTS)).safeDrop();
    await mongo(db.collection(TEACHERS)).safeDrop();
    await db.createCollection(STUDENTS);
    await db.collection(STUDENTS).insertMany(aggreagationData);
};

const prepareCommand = command => `"${command.replace(/\s{2,}/g, '').replace(/"/g, "'")}"`;

const execute = async (command, isArray = true) => {
    try {
        if (!command.match(/^\s*db\.(\w+|getCollection\(.\w+.\))\./)) return {};
        const result = (await cmd(`mongo\\mongo.exe --eval ${prepareCommand(command)}`))
            .split(/\r?\n/)
            .slice(4, -1)
            .join()
            .replace(/WriteResult\((.*)\)/, '$1');
        return JSON.parse(isArray ? `[${result}]` : result);
    } catch (e) {
        return {};
    }
};

const findSortedArrayWithoutIds = async collection =>
    await mongo(collection).find({}, { sort: { name: 1 }, projection: { _id: 0 } });

const checkCrudQ1 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        const studentsCol = db.collection(STUDENTS);
        await initDBForCrudQuestion(db);
        await execute(solutions.Question1.Q_1);
        const q1Solution = await studentsCol.findOne(
            { name: 'Raphael' },
            { projection: { _id: 0 } }
        );
        await initDBForCrudQuestion(db);
        await execute(answers.Question1.Q_1);
        const q1Answer = await studentsCol.findOne({ name: 'Raphael' }, { projection: { _id: 0 } });
        grade += Number(q1Solution.department == q1Answer.department) * (scores.crud1 / 4.0);
        grade += Number(equals(q1Solution.courses, q1Answer.courses)) * (scores.crud1 / 4.0);
        grade += Number(q1Solution.city == q1Answer.city) * (scores.crud1 / 4.0);
        grade += Number(equals(q1Solution.teachers, q1Answer.teachers)) * (scores.crud1 / 4.0);
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkCrudQ2 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        const teachersCol = db.collection(TEACHERS);
        await initDBForCrudQuestion(db);
        await teachersCol.deleteOne({});
        const q2Solution = await execute(solutions.Question1.Q_2, false);
        await initDBForCrudQuestion(db);
        await teachersCol.deleteOne({});
        const q2Answer = await execute(answers.Question1.Q_2, false);
        grade = Number(q2Solution == q2Answer) * scores.crud2;
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkCrudQ3 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        await initDBForCrudQuestion(db);
        const q3Solution = await execute(solutions.Question1.Q_3, false);
        await initDBForCrudQuestion(db);
        const q3Answer = await execute(answers.Question1.Q_3, false);
        grade = Number(q3Solution == q3Answer) * scores.crud3;
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkCrudQ4 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        const studentsCol = db.collection(STUDENTS);
        await initDBForCrudQuestion(db);
        await execute(solutions.Question1.Q_4, false);
        const q4Solution = await studentsCol.findOne({ name: 'Neo' }, { projection: { _id: 0 } });
        await initDBForCrudQuestion(db);
        await execute(answers.Question1.Q_4, false);
        const q4Answer = await studentsCol.findOne({ name: 'Neo' }, { projection: { _id: 0 } });
        grade = Number(equals(q4Solution, q4Answer)) * scores.crud4;
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkCrudQ5 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        const studentsCol = db.collection(STUDENTS);
        await initDBForCrudQuestion(db);
        await execute(solutions.Question1.Q_5, false);
        const q5Solution = await studentsCol.findOne({ name: 'Luke' }, { projection: { _id: 0 } });
        await initDBForCrudQuestion(db);
        await execute(answers.Question1.Q_5, false);
        const q5Answer = await studentsCol.findOne({ name: 'Luke' }, { projection: { _id: 0 } });
        grade = Number(equals(q5Solution, q5Answer)) * scores.crud5;
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkCrudQ6 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        const studentsCol = db.collection(STUDENTS);
        await initDBForCrudQuestion(db);
        await execute(solutions.Question1.Q_1, false);
        await execute(solutions.Question1.Q_6, false);
        const q6Solution = await studentsCol.findOne(
            { name: 'Raphael' },
            { projection: { _id: 0 } }
        );
        await initDBForCrudQuestion(db);
        await execute(solutions.Question1.Q_1, false);
        await execute(answers.Question1.Q_6, false);
        const q6Answer = await studentsCol.findOne({ name: 'Raphael' }, { projection: { _id: 0 } });
        grade = Number(equals(q6Solution, q6Answer)) * scores.crud6;
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkCrudQ7 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        const studentsCol = db.collection(STUDENTS);
        await initDBForCrudQuestion(db);
        await execute(solutions.Question1.Q_7, false);
        const q7Solution = await findSortedArrayWithoutIds(studentsCol);
        await initDBForCrudQuestion(db);
        await execute(answers.Question1.Q_7, false);
        const q7Answer = await findSortedArrayWithoutIds(studentsCol);
        grade = Number(equals(q7Solution, q7Answer)) * scores.crud7;
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkCrudQ8 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        const studentsCol = db.collection(STUDENTS);
        const teachersCol = db.collection(TEACHERS);
        const apply = async commands => {
            await initDBForCrudQuestion(db);
            await execute(commands.Question1.Q_8[0], false);
            await execute(commands.Question1.Q_8[1], false);
            await execute(commands.Question1.Q_8[2], false);
            const students = await findSortedArrayWithoutIds(studentsCol);
            const teachers = await findSortedArrayWithoutIds(teachersCol);
            return { students, teachers };
        };
        const { students: q8SolutionStudents, teachers: q8SolutionTeachers } = await apply(
            solutions
        );
        const { students: q8AnswerStudents, teachers: q8AnswerTeachers } = await apply(answers);
        grade += Number(equals(q8SolutionStudents, q8AnswerStudents)) * ((scores.crud8 / 5.0) * 3);
        grade += Number(equals(q8SolutionTeachers, q8AnswerTeachers)) * ((scores.crud8 / 5.0) * 2);
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkCrudQ9 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        const studentsCol = db.collection(STUDENTS);
        await initDBForCrudQuestion(db);
        await execute(solutions.Question1.Q_9, false);
        const q9Solution = await findSortedArrayWithoutIds(studentsCol);
        await initDBForCrudQuestion(db);
        await execute(answers.Question1.Q_9, false);
        const q9Answer = await findSortedArrayWithoutIds(studentsCol);
        grade = Number(equals(q9Solution, q9Answer)) * scores.crud9;
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkCrudQ10 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        const studentsCol = db.collection(STUDENTS);
        await initDBForCrudQuestion(db);
        await execute(solutions.Question1.Q_10, false);
        const q10Solution = await findSortedArrayWithoutIds(studentsCol);
        await initDBForCrudQuestion(db);
        await execute(answers.Question1.Q_10, false);
        const q10Answer = await findSortedArrayWithoutIds(studentsCol);
        grade = Number(equals(q10Solution, q10Answer)) * scores.crud10;
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkAggregationQ1 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        await initDBForAggregationQuestion(db);
        const q1Solution = await execute(solutions.Question2.Q_1);
        await initDBForAggregationQuestion(db);
        const q1Answer = await execute(answers.Question2.Q_1);
        const sort = sortBy(prop('city'));
        grade += Number(equals(sort(q1Solution), sort(q1Answer))) * scores.aggregation1;
        grade +=
            grade != 0
                ? 0
                : Number(all(elem => contains(elem, q1Answer))(q1Solution)) *
                  (scores.aggregation1 / 2);
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkAggregationQ2 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        await initDBForAggregationQuestion(db);
        const q2Solution = await execute(solutions.Question2.Q_2);
        await initDBForAggregationQuestion(db);
        const q2Answer = await execute(answers.Question2.Q_2);
        const sort = sortBy(prop('teacher'));
        grade += Number(equals(sort(q2Solution), sort(q2Answer))) * scores.aggregation2;
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkAggregationQ3 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        await initDBForAggregationQuestion(db);
        const q3Solution = await execute(solutions.Question2.Q_3);
        await initDBForAggregationQuestion(db);
        const q3Answer = await execute(answers.Question2.Q_3);

        /*
           we need to think about the case that:
           { "students_count": 95, "department": "department_5" },
           { "students_count": 95, "department": "department_1" }
           one time we can get department_5 before department_1 and the opposite.. 
           we will solve that with groupBy and with check that it's really with descending order
        */
        const groupByCounts = array => {
            return pipe(
                groupBy(prop('students_count')),
                map(
                    pipe(
                        pluck('department'),
                        sort((dep1, dep2) => dep1.localeCompare(dep2))
                    )
                )
            )(array);
        };
        const counts = pluck('students_count');
        grade +=
            Number(equals(groupByCounts(q3Solution), groupByCounts(q3Answer))) *
            (scores.aggregation3 / 2);
        grade += Number(equals(counts(q3Solution), counts(q3Answer))) * (scores.aggregation3 / 2);
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkAggregationQ4 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        await initDBForAggregationQuestion(db);
        const q4Solution = await execute(solutions.Question2.Q_4);
        await initDBForAggregationQuestion(db);
        const q4Answer = await execute(answers.Question2.Q_4);
        const prepareToCompare = pipe(
            groupBy(prop('course')),
            map(
                pipe(
                    head,
                    prop('average'),
                    parseInt
                )
            )
        );
        const preparedQ4Solution = prepareToCompare(q4Solution);
        const preparedQ4Answer = prepareToCompare(q4Answer);
        const withoutCourse9 = dissoc('course_9');
        grade += Number(equals(preparedQ4Solution, preparedQ4Answer)) * (scores.aggregation4 / 2);
        grade +=
            Number(equals(withoutCourse9(preparedQ4Solution), withoutCourse9(preparedQ4Answer))) *
            (scores.aggregation4 / 2);
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkAggregationQ5 = async (db, solutions, answers) => {
    let grade = 0;
    try {
        await initDBForAggregationQuestion(db);
        const q5Solution = await execute(solutions.Question2.Q_5);
        await initDBForAggregationQuestion(db);
        const q5Answer = await execute(answers.Question2.Q_5);
        const sort = sortBy(prop('average'));
        grade += Number(equals(sort(q5Solution), sort(q5Answer))) * scores.aggregation5;
    } catch (e) {
    } finally {
        return grade;
    }
};

const checkExam = db => async studentAnswers => {
    const gradeDetails = {
        crud1: await checkCrudQ1(db, solutions, studentAnswers),
        crud2: await checkCrudQ2(db, solutions, studentAnswers),
        crud3: await checkCrudQ3(db, solutions, studentAnswers),
        crud4: await checkCrudQ4(db, solutions, studentAnswers),
        crud5: await checkCrudQ5(db, solutions, studentAnswers),
        crud6: await checkCrudQ6(db, solutions, studentAnswers),
        crud7: await checkCrudQ7(db, solutions, studentAnswers),
        crud8: await checkCrudQ8(db, solutions, studentAnswers),
        crud9: await checkCrudQ9(db, solutions, studentAnswers),
        crud10: await checkCrudQ10(db, solutions, studentAnswers),
        aggregation1: await checkAggregationQ1(db, solutions, studentAnswers),
        aggregation2: await checkAggregationQ2(db, solutions, studentAnswers),
        aggregation3: await checkAggregationQ3(db, solutions, studentAnswers),
        aggregation4: await checkAggregationQ4(db, solutions, studentAnswers),
        aggregation5: await checkAggregationQ5(db, solutions, studentAnswers)
    };
    return gradeDetails;
};

module.exports = async (db, answersFileName) => {
    const studentAnswers = require(`../${answersFolder}/${answersFileName}`);
    const results = await checkExam(db)(studentAnswers);
    return results;
};
